package taller.interfaz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import taller.estructuras.Lista;
import taller.estructuras.TablaHash;
import taller.estructuras.TablaHash2;
import taller.mundo.Habitante;


public class Main {

	private static String rutaEntrada = "./data/ciudadLondres.csv";
	private static TablaHash<Integer, Habitante> t ;
	private static TablaHash2<String, Habitante> t2;
	private static TablaHash2<String, Habitante> t3;

	public static void main(String[] args) {
		//Cargar registros
		System.out.println("Bienvenido al directorio de emergencias por colicsiones de la ciudad de Londres");
		System.out.println("Espere un momento mientras cargamos la información...");
		System.out.println("Esto puede tardar unos minutos...");
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(rutaEntrada)));
			String entrada = br.readLine();

			//TODO: Inicialice el directorio t
			t = new TablaHash<Integer,Habitante>();
			t2 = new TablaHash2<String,Habitante>();
			t3 = new TablaHash2<String,Habitante>();


			int i = 0;
			entrada = br.readLine();
			while (entrada != null){
				String[] datos = entrada.split(",");
				//TODO: Agrege los datos al directorio de emergencias
				//Recuerde revisar en el enunciado la estructura de la información

				Habitante hab = new Habitante(datos[0],datos[1],datos[2],datos[3]);
				t.put(Integer.parseInt(datos[2]),hab);
				t2.put(datos[0], hab);
				t3.put(datos[3],hab);
				System.out.println(i);
				++i;
				if (++i%500000 == 0)
					System.out.println(i+" entradas...");

				entrada = br.readLine();
			}
			System.out.println(i+" entradas cargadas en total");
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Directorio de emergencias por colisiones de Londres v1.0");

		boolean seguir = true;

		while (seguir)
			try {
				System.out.println("Bienvenido, seleccione alguna opcion del menú a continuación:");
				System.out.println("1: Agregar ciudadano a la lista de emergencia");
				System.out.println("2: Buscar ciudadano por número de contacto del acudiante");
				System.out.println("3: Buscar ciudadano por nombre");
				System.out.println("4: Buscar ciudadano por su locaclización actual");
				System.out.println("Exit: Salir de la aplicación");

				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				String in = br.readLine();
				switch (in) {
				case "1":
					//TODO: Implemente el requerimiento 1.
					//Agregar ciudadano a la lista de emergencia
					System.out.println("Ingrese el nombre del cidudadano");
					String nombre = br.readLine();
					System.out.println("Ingrese el apellido del cidudadano");
					String apellido = br.readLine();
					System.out.println("Ingrese el telefono del cidudadano");
					String tel = br.readLine();
					System.out.println("Ingrese la localizacion del cidudadano");
					String local = br.readLine();

					Habitante hab = new Habitante(nombre, apellido, tel, local);

					t.put(Integer.parseInt(tel), hab);


					break;
				case "2":
					//TODO: Implemente el requerimiento 2
					//Buscar un ciudadano por el número de teléfono de su acudiente
					System.out.println("Ingrese el telefono del ciudadano a buscar");
					String telefono = br.readLine();
					Habitante habitanteTel = t.get(Integer.parseInt(telefono));
					if(habitanteTel!=null)
					{
						System.out.println(habitanteTel.toString());
					}
					else
					{
						System.out.println("El ciudadano no existe en la base de datos");
					}
					break;
				case "3":
					//TODO: Implemente el requerimiento 3
					//Buscar ciudadano por apellido/nombre
					System.out.println("Ingrese el nombre del ciudadano a buscar");
					String nombreBuscado = br.readLine();
					Lista<Habitante> lta = t2.get(nombreBuscado);
					if(lta!=null)
					{
						System.out.println("La siguiente es la lista de los ciudadanos con ese nombre");
						System.out.println(lta.toString());
					}
					else
					{
						System.out.println("El ciudadano no existe en la base de datos");
					}

					break;
				case "4":
					//TODO: Implemente el requerimiento 4
					//Buscar ciudadano por su locaclización actual
					System.out.println("Ingrese la localizacion del ciudadano a buscar");
					String localizacion = br.readLine();
					Lista<Habitante> lista2 = t3.get(localizacion);
					if(lista2!=null)
					{
						System.out.println("La siguiente es la lista de los ciudadanos en esa localizacion");
						System.out.println(lista2.toString());
					}
					else
					{
						System.out.println("El ciudadano no existe en la base de datos");
					}
					break;
				case "Exit":
					System.out.println("Cerrando directorio...");
					seguir = false;
					break;

				default:
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}


	}

}
