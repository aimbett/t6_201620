package taller.estructuras;

public class NodoHash2<K extends Comparable<K>,V> {

	private K llave;
	private Lista<V> valores;
	
	
	public NodoHash2(K llave, V valor) {
		super();
		this.llave = llave;
		valores = new Lista<V>();
		valores.agregarPush(valor);
	}
	
	public K getLlave() {
		return llave;
	}
	
	public void setLlave(K llave) {
		this.llave = llave;
	}
	
		
	public void addValor(V valor) {
		valores.agregarPush(valor);
	}

	public Lista<V> getValores() {
		return valores;
	}


	
}
