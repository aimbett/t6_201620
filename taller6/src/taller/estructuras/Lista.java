package taller.estructuras;

import java.util.Iterator;



public class Lista<T> {

	private int tamanio ;

	private Nodo<T> primero;
	private Nodo<T> ultimo;

	public Lista(){
		tamanio = 0;
		primero = null;
		ultimo = null;
	}


	public T pop(){
		T temp =primero.getValor();
		primero = primero.getSiguiente();
		tamanio--;
		return temp;
	}
	public Nodo<T> darPrimero(){
		return primero;
	}
	
	public void agregarPush( T objeto){

		Nodo<T> temp = primero;
		primero = new Nodo<T>(objeto);
		primero.setSiguiente(temp);
		tamanio++;

	}
	public int darTamanio(){
		return tamanio;
	}

	//Cuando pertenezca a una cola.
	public void encolar(T objeto){
		Nodo<T> nuevo = new Nodo<T>(objeto);
		if( primero == null){

			primero = nuevo;
			
			ultimo = nuevo;
		}
		else{
			ultimo.setSiguiente(nuevo);
			ultimo = nuevo;
		}
		tamanio ++;
	}

	public T desencolar(){
		T retorno = primero.getValor();
		primero = primero.getSiguiente();
		tamanio--;
		return retorno;

	}
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new IteratorFila();
	}


	private class IteratorFila implements Iterator<T>{

		private Nodo<T> actual = primero;

		public boolean hasNext() {
			// TODO Auto-generated method stub
			return actual!=null;
		}

		public T next() {
			// TODO Auto-generated method stub
			T ret = actual.getValor();
			actual = actual.getSiguiente();
			return ret;
		}

		public void remove() {
			// TODO Auto-generated method stub

		}
	}
	
	public String toString()
	{
		String resp = "";
		Iterator<T> it = iterator();
		while(it.hasNext()){
			T elem = it.next();
			resp+=elem.toString()+"\n\n";
		}
		return resp;
	}
}
