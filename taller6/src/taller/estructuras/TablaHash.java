package taller.estructuras;

import taller.mundo.Habitante;

public class TablaHash<K extends Comparable<K> ,V> {

	//TODO Una enumeracioón que representa los tipos de colisiones que se pueden manejar

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	//TODO: Agruegue la estructura que va a soportar la tabla.
	private NodoHash<K, V> [] tabla;

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	//Constructores

	@SuppressWarnings("unchecked")
	public TablaHash(){
		//TODO: Inicialice la tabla con los valores que considere prudentes para una ejecución normal
		this(500000,5);
	}

	@SuppressWarnings("unchecked")
	public TablaHash(int capacidad, float factorCargaMax) {
		//TODO: Inicialice la tabla con los valores dados por parametro
		this.factorCargaMax = factorCargaMax;
		this.capacidad = capacidad;
		count = 0;
		tabla = new NodoHash[capacidad];

	}

	public void put(K llave, V valor){
		//TODO: Gaurde el objeto valor dado por parametro el cual tiene la llave,
		//tenga en cuenta que puede o no puede haber colisiones
		int pos = hash(llave);
		
		NodoHash<K, V> nodo = tabla[pos];
		if(nodo==null)
		{
			tabla[pos] = new NodoHash<K, V>(llave, valor);
		}
		else
		{
			if(nodo.getLlave().compareTo(llave)==0){
				nodo.setValor(valor);
				return;
			}
			while(nodo.getNext()!=null)
			{
				if(nodo.getNext().getLlave().compareTo(llave)==0)
				{
					nodo.getNext().setValor(valor);
					return;
				}
				nodo = nodo.getNext();
			}
			nodo.setNext(new NodoHash<K, V>(llave, valor));
		}
		count++;

	}

	public V get(K llave){
		//TODO: Busque y retorne el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		int pos = hash(llave);
		NodoHash<K, V> nodo = tabla[pos];
		if(nodo==null)
		{
			return null;
		}
		else 
		{
			if(nodo.getLlave().compareTo(llave)==0)
			{
				return nodo.getValor();
			}
			while(nodo.getNext()!=null)
			{
				if(nodo.getLlave().compareTo(llave)==0)
				{
					return nodo.getValor();
				}
				nodo = nodo.getNext();
			}

		}

		return null;
	}

	public V delete(K llave){
		//TODO: borra el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		V resp = null;
		int pos = hash(llave);
		NodoHash<K, V> nodo = tabla[pos];
		NodoHash<K, V> anterior = null;

		if(nodo==null)
		{
			return null;
		}
		else 
		{
			if(nodo.getLlave().compareTo(llave)==0)
			{
				count--;
				tabla[pos]= nodo.getNext();
				return nodo.getValor();
			}
			while(nodo.getNext()!=null)
			{

				if(nodo.getLlave().compareTo(llave)==0)
				{	
					count --;
					resp = nodo.getValor();
					if(anterior != null)
					{
						anterior.setNext(nodo.getNext());
					}
				}
				anterior = nodo;
				nodo = nodo.getNext();
			}

		}

		return resp;
	}

	//Hash
	private int hash(K llave)
	{
		//TODO: Escriba una función de Hash, recuerde tener en cuenta la complejidad de ésta así como las colisiones.

		return llave.hashCode()%capacidad;
	}

	//TODO: Permita que la tabla sea dinamica
	public static void main(String[] args) {
		TablaHash<Integer, Habitante> t = new TablaHash<Integer,Habitante>();
		for(int i =1 ;i<=10;i++)
		{
			Habitante hab = new Habitante("N", "A", "310000"+i, "L");
			t.put(Integer.parseInt(hab.getTelefono()), hab);
			
		}
		System.out.println("El numero de elementos debe ser 10 y es de: "+t.count);
		System.out.println();
		System.out.println();
		System.out.println("Estado de la tabla:");
		for(int i=0 ; i<t.tabla.length;i++)
		{
			NodoHash nodo = t.tabla[i];
			if(nodo!=null)
			{
				Habitante hab = (Habitante) nodo.getValor();
				String telefono = hab.getTelefono();
				System.out.println("En la posicion: "+i+" de la lista se encuentra el habitante con telefono: "+telefono);
			}
		}
		
		System.out.println();
		System.out.println();
		System.out.println("Busqueda de un elemento:");
		Habitante habBuscado = t.get(3100002);
		System.out.println("El habitante con llave 3100002 tiene el siguiente telefono: "+ habBuscado.getTelefono());
	}

}