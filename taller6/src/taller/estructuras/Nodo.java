package taller.estructuras;

public class Nodo<T> {
	private Nodo<T> siguiente;

	private T valor;

	public Nodo(T pValor){
		valor = pValor;
		siguiente = null;
	}

	public Nodo<T> getSiguiente() {
		return siguiente;
	}

	public void setSiguiente(Nodo<T> pSiguiente) {
		this.siguiente = pSiguiente;
	}

	public T getValor() {
		return valor;
	}

	public void setValor(T pValor) {
		this.valor = pValor;
	}
}
