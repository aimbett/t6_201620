package taller.estructuras;

import java.awt.List;
import java.util.Iterator;

import taller.mundo.Habitante;

public class TablaHash2<K extends Comparable<K> ,V> {

	//TODO Una enumeracioón que representa los tipos de colisiones que se pueden manejar

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	//TODO: Agruegue la estructura que va a soportar la tabla.
	private NodoHash2<K,V>[] tabla;

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	//Constructores

	@SuppressWarnings("unchecked")
	public TablaHash2(){
		//TODO: Inicialice la tabla con los valores que considere prudentes para una ejecución normal
		this(500000,5);
	}

	@SuppressWarnings("unchecked")
	public TablaHash2(int capacidad, float factorCargaMax) {
		//TODO: Inicialice la tabla con los valores dados por parametro
		this.factorCargaMax = factorCargaMax;
		this.capacidad = capacidad;
		count = 0;
		tabla = new NodoHash2[capacidad];

	}

	public void put(K llave, V valor){
		//TODO: Gaurde el objeto valor dado por parametro el cual tiene la llave,
		//tenga en cuenta que puede o no puede haber colisiones
		int pos = hash(llave);
	

		boolean agrego = false;
		for(int i = pos;i<tabla.length&&!agrego;i++)
		{
			NodoHash2<K, V> nodo = tabla[i];
			if(nodo == null )
			{
				
				tabla[i] = new NodoHash2<K,V>(llave, valor);
				agrego = true;
			}
			else if(nodo.getLlave().compareTo(llave)==0)
			{
				nodo.addValor(valor);
				agrego =true;
			}
		}
		if(!agrego)
		{
			for(int i = 0;i<pos&&!agrego;i++)
			{
				NodoHash2<K, V> nodo = tabla[i];
				if(nodo == null )
				{
					nodo = new NodoHash2<K,V>(llave, valor);
					agrego = true;
				}
				else if(nodo.getLlave().compareTo(llave)==0)
				{
					nodo.addValor(valor);
					agrego =true;
				}
			}
		}


		count++;

	}

	public Lista<V> get(K llave){
		//TODO: Busque y retorne el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		int pos = hash(llave);
		boolean encontro = false;
		for(int i = pos;i<tabla.length&&!encontro;i++)
		{
			NodoHash2<K, V> nodo = tabla[i];
			if(nodo!=null)
			{
				if(nodo.getLlave().compareTo(llave)==0)
				{
					encontro = true;
					return nodo.getValores();
				}	
			}

		}
		if(!encontro)
		{
			for(int i = 0;i<pos&&!encontro;i++)
			{
				NodoHash2<K, V> nodo = tabla[i];
				if(nodo!=null)
				{
					if(nodo.getLlave().compareTo(llave)==0)
					{
						encontro = true;
						return nodo.getValores();
					}	
				}
			}
		}

		return null;
	}

	public Lista<V> delete(K llave){
		//TODO: borra el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		Lista<V> resp = null;
		int pos = hash(llave);
		boolean encontro = false;
		for(int i = pos;i<tabla.length&&!encontro;i++)
		{
			NodoHash2<K, V> nodo = tabla[i];
			if(nodo.getLlave().compareTo(llave)==0)
			{
				encontro = true;

				resp =nodo.getValores();
				nodo = null;
			}
		}
		if(!encontro)
		{
			for(int i = 0;i<pos&&!encontro;i++)
			{
				NodoHash2<K, V> nodo = tabla[i];
				if(nodo.getLlave().compareTo(llave)==0)
				{
					encontro = true;
					resp= nodo.getValores();
					nodo = null;
				}
			}
		}

		return resp;
	}


	//Hash
	private int hash(K llave)
	{
		//TODO: Escriba una función de Hash, recuerde tener en cuenta la complejidad de ésta así como las colisiones.

		return Math.abs(llave.hashCode()%capacidad);
	}

	//TODO: Permita que la tabla sea dinamica
	public static void main(String[] args) {
		TablaHash2<String, Habitante> t = new TablaHash2<String,Habitante>();
		for(int i =1 ;i<=10;i++)
		{
			Habitante hab = new Habitante("N", "A", "310000"+i, "L1");
			t.put(hab.getLocalizacion(), hab);

		}
		for(int i =1 ;i<=10;i++)
		{
			Habitante hab = new Habitante("N", "A", "320000"+i, "L2");
			t.put(hab.getLocalizacion(), hab);

		}
		System.out.println("El numero de elementos debe ser 20 y es de: "+t.count);
		System.out.println();
		System.out.println();
		System.out.println("Estado de la tabla:");
		for(int i=0 ; i<t.tabla.length;i++)
		{
			NodoHash2<String, Habitante> nodo = t.tabla[i];
			if(nodo!=null)
			{
				System.out.println(":)");
				Lista<Habitante> lista = nodo.getValores();
				Iterator it = lista.iterator();
				System.out.println("En la posicion: "+i+" de la lista se encuentran los habitantes con telefonos: ");

				while(it.hasNext())
				{
					Habitante h = (Habitante) it.next();
					System.out.println("\t -"+h.getTelefono());

				}
			}
		}

		System.out.println();
		System.out.println();
		System.out.println("Busqueda de un elemento:");
		Lista<Habitante> habBuscado = t.get("L1");
	}

}