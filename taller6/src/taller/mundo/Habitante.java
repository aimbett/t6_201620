package taller.mundo;

public class Habitante {

	private String nombre;
	
	private String apellido;
	
	private String telefono;
	
	private String localizacion;
	
	public Habitante(String pNombre, String pApellido, String pTelefono, String pLocalizacion)
	{
		nombre = pNombre;
		apellido = pApellido;
		telefono = pTelefono;
		localizacion = pLocalizacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getLocalizacion() {
		return localizacion;
	}

	public void setLocalizacion(String localizacion) {
		this.localizacion = localizacion;
	}
	
	public String toString()
	{
		return "Nombre: "+nombre+"\nApellido: "+apellido+"\nTelefono; "+telefono+"\nLocalizacion: "+localizacion;
	}
}
